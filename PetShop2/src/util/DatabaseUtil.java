package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.sql.Date;
import java.util.List;

import model.Animal;
import model.Personalmedical;
import model.Programare;

/**
 * @author Cristi
 *
 */
public class DatabaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public void setUp() {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShop2");
		entityManager = entityManagerFactory.createEntityManager();
	}
	public void saveAnimal (Animal animal) {
		entityManager.persist(animal);
	}
	public void savePersonalMedical (Personalmedical personalMedical) {
			entityManager.persist(personalMedical);
	}
	public void saveProgramare (Programare programare) {
		entityManager.persist(programare);
}
	public void beginTransaction() {
		entityManager.getTransaction().begin();
	}
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	public void closeEntityManager() {
		entityManager.close();
	}
	public static void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.Animal", Animal.class).getResultList();
		for (Animal animal : results) {
			 System.out.println("Animal :" + animal.getNumeAnimal() + " has ID: "+ animal.getIdAnimal());
		}
	}
	public static void printAllPersonalmedicalFromDB() {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from petshop.Personalmedical", Personalmedical.class).getResultList();
		for (Personalmedical personalMedical : results) {
			 System.out.println("PersonalMedical:" + personalMedical.getNumePersonalMedical() + " has ID: "+ personalMedical.getIdPersonalMedical());
		}
	}
		public static void printAllProgramareFromDB() {
			List<Programare> results = entityManager.createNativeQuery("Select * from petshop.Programare", Programare.class).getResultList();
			for (Programare programare : results) {
				 System.out.println("Programare :" + programare.getIdProgramare() + " "+ programare.getDataProgramare() + " " + programare.getGradUrgenta());
			}
		}
	public List<Animal> animalList() { 
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}   
	
	public static void createAnimal(int idAnimal, String numeAnimal, String specie, int varsta, int masa, String numeOwner, String telefon)
            throws Exception {

        DatabaseUtil databaseUtil = new DatabaseUtil();
        Animal animal = new Animal();

        animal.setIdAnimal(idAnimal);
        animal.setNumeAnimal(numeAnimal);
        animal.setSpecie(specie);
        animal.setVarsta(varsta);
        animal.setMasa(masa);
        animal.setNumeOwner(numeOwner);
        animal.setTelefon(telefon);

        databaseUtil.setUp();
        databaseUtil.beginTransaction();
        databaseUtil.saveAnimal(animal);
        databaseUtil.commitTransaction();
        System.out.println("Animal adaugat.");
        databaseUtil.closeEntityManager();
    }
	
	public static void createPersonalMedical(int idPersonalMedical, String numePersonalMedical, int varstaPersonalMedical, String telefon)
            throws Exception {

        DatabaseUtil databaseUtil = new DatabaseUtil();
        Personalmedical personalMedical = new Personalmedical();

        personalMedical.setIdPersonalMedical(idPersonalMedical);
        personalMedical.setNumePersonalMedical(numePersonalMedical);
        personalMedical.setVarstaPersonalMedical(varstaPersonalMedical);
        personalMedical.setTelefon(telefon);

        databaseUtil.setUp();
        databaseUtil.beginTransaction();
        databaseUtil.savePersonalMedical(personalMedical);
        databaseUtil.commitTransaction();
        System.out.println("Personal medical adaugat.");
        databaseUtil.closeEntityManager();
    }
	
	public static void createProgramare(int idProgramare, Date dataProgramare, int idAnimal, int idPersonalMedical, String gradUrgenta)
            throws Exception {

        DatabaseUtil databaseUtil = new DatabaseUtil();
        Programare programare = new Programare();

        Animal animal = entityManager.find(Animal.class, idAnimal);
        Personalmedical personalMedical = entityManager.find(Personalmedical.class, idPersonalMedical);
        
        programare.setIdProgramare(idProgramare);
        programare.setAnimal(animal);
        programare.setPersonalmedical(personalMedical);
        programare.setDataProgramare(dataProgramare);
        programare.setGradUrgenta(gradUrgenta);

        databaseUtil.setUp();
        databaseUtil.beginTransaction();
        databaseUtil.saveProgramare(programare);
        databaseUtil.commitTransaction();
        System.out.println("Programare adaugata.");
        databaseUtil.closeEntityManager();
    }

	public static void editAnimalByID(int idAnimal, String numeAnimal, String specie, int varsta, int masa, String numeOwner, String telefon)
			throws Exception {
		
		Animal animal = entityManager.find(Animal.class, idAnimal);
		entityManager.getTransaction().begin();
		
        animal.setIdAnimal(idAnimal);
        animal.setNumeAnimal(numeAnimal);
        animal.setSpecie(specie);
        animal.setVarsta(varsta);
        animal.setMasa(masa);
        animal.setNumeOwner(numeOwner);
        animal.setTelefon(telefon);
		
		entityManager.getTransaction().commit();
	}
	

	public static void editPersonalByID(int idPersonalMedical, String numePersonalMedical, int varstaPersonalMedical, String telefon)
			throws Exception {
		
		Personalmedical personalMedical = entityManager.find(Personalmedical.class, idPersonalMedical);
		entityManager.getTransaction().begin();
		
        personalMedical.setIdPersonalMedical(idPersonalMedical);
        personalMedical.setNumePersonalMedical(numePersonalMedical);
        personalMedical.setVarstaPersonalMedical(varstaPersonalMedical);
        personalMedical.setTelefon(telefon);
		entityManager.getTransaction().commit();
	}
	
	public static void editProgramareByID(int idProgramare, Date dataProgramare, int idAnimal, int idPersonalMedical, String gradUrgenta)
			throws Exception {
		
		Programare programare = entityManager.find(Programare.class, idProgramare);
		entityManager.getTransaction().begin();
		
		programare.setIdProgramare(idProgramare);
		programare.getAnimal().setIdAnimal(idAnimal);
		programare.getPersonalmedical().setIdPersonalMedical(idPersonalMedical);
        programare.setDataProgramare(dataProgramare);
        programare.setGradUrgenta(gradUrgenta);
        
		
		entityManager.getTransaction().commit();
	}
	
	public static void removeAnimalByID(int idAnimal) {
		Animal animal = entityManager.find(Animal.class, idAnimal);
		entityManager.getTransaction().begin();
		
		entityManager.remove(animal);
		
		entityManager.getTransaction().commit();
		
	}
	

	public static void removePersonalByID(int idPersonal) {
		

		Personalmedical personalMedical = entityManager.find(Personalmedical.class, idPersonal);
		entityManager.getTransaction().begin();
		
		entityManager.remove(personalMedical);
		
		entityManager.getTransaction().commit();

	}
	

	public static void removeProgramareByID(int idProgramare) {
		

		
		Programare programare = entityManager.find(Programare.class, idProgramare);
		entityManager.getTransaction().begin();
		
		entityManager.remove(programare);
		
		entityManager.getTransaction().commit();
		
	}

	
	}
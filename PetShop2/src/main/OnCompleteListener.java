package main;

public interface OnCompleteListener {
    public void onComplete() throws Exception;
}

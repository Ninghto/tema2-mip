package main;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Personalmedical;
import util.DatabaseUtil;


/**
 * @author Cristi
 *
 */
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/Login.fxml"));
			Scene scene = new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		try {
            ServerSocket serverSocket = new ServerSocket(5000);
            while (true) {
                new echoServer.Echoer(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
		
	}

	public static void main(String[] args) throws Exception {
		launch(args);
		int direction = -1;
		System.out.println("Where are you going? ");
		Scanner location = new Scanner(System.in);
		direction = location.nextInt();
		if (direction == 1)
			launch(args);
		else if(direction == 2){
			List<Animal> animals = new ArrayList<>();
			Animal a1 = new Animal(0,"Rex", "Caine", 2, 5, "Marian", "0759456963");
			Animal a2 = new Animal(1,"Pik", "Pisica", 1, 2, "Marian", "0759456963");
			Animal a3 = new Animal(2,"Pac", "Caine", 1, 1, "Marian", "0759456963");
			 animals.add(a1);
		     animals.add(a2);
		     animals.add(a3);
			
			Collections.sort(animals, (animal1,animal2) -> animal1.getNumeAnimal().compareTo(animal2.getNumeAnimal()));
		     for(Animal a : animals) {
		            System.out.println(a.getNumeAnimal());
		        }
		}
		
		else if(direction == 3) {
		int choice = -1;
        while (choice != 0) {
            @SuppressWarnings("resource")
            Scanner keyboard = new Scanner(System.in);
 
            System.out.println("0. Exit");
            System.out.println("1. Create Animal");
            System.out.println("2. Create Person");
            System.out.println("3. Create Programare");
            System.out.println("4. Edit Animal by id");
            System.out.println("5. Edit Person by id");
            System.out.println("6. Edit Programare by id");
            System.out.println("7. Remove Animal by id");
            System.out.println("8. Remove Person by id");
            System.out.println("9. Remove Programare by id");
            System.out.println("10. Print Animal by id");
            System.out.println("11. Print Person by id");
            System.out.println("12. Print Programare by id");
            System.out.println("13. Print Sorted Animals by name");
            System.out.println("Choose an Option: ");
            choice = keyboard.nextInt();
 
            switch (choice) {
            case 0:
                break;
            case 1:
                System.out.println("(idAnimal, numeAnimal, specie, varsta, masa, numeOwner, telefon");
                int idAnimal = keyboard.nextInt();
                String numeAnimal = keyboard.next();
                String specie = keyboard.next();
                int varsta = keyboard.nextInt();
                int masa = keyboard.nextInt();
                String numeOwner = keyboard.next();
                String telefon = keyboard.next();
 
                //Listener
                LongRunningTask longRunningTask = new LongRunningTask();
 
                longRunningTask.setOnCompleteListener(new OnCompleteListener() {
 
                    @Override
                    public void onComplete() {
                        System.out.println("Animal added.");
 
                    }
                });
                System.out.println("Starting to add the Animal into the DataBase.");
                longRunningTask.run();
               
                DatabaseUtil.createAnimal(idAnimal, numeAnimal, specie, varsta, masa, numeOwner, telefon);
                break;
 
            case 2:
                System.out.println("int idPersonalMedical, String numePersonalMedical, int varstaPersonalMedical, String telefon");
                int idPersonalMedical = keyboard.nextInt();
                String numePersonalMedical = keyboard.next();
                int varstaPersonalMedical = keyboard.nextInt();
                String telefon2 = keyboard.next();
 
                DatabaseUtil.createPersonalMedical(idPersonalMedical, numePersonalMedical, varstaPersonalMedical, telefon2);
 
            case 3:
 
                System.out.println("int idProgramare, Date dataProgramare, int idAnimal, int idPersonalMedical, String gradUrgenta");
                int idProgramare = keyboard.nextInt();
              //  Date dataProgramare2 = keyboard.nextLine();
                int idAnimal2 = keyboard.nextInt();
                int idPersonalMedical2 = keyboard.nextInt();
                String gradUrgenta= keyboard.next();
 
                Date dataProgramare2 = null;
				DatabaseUtil.createProgramare(idProgramare, dataProgramare2, idAnimal2, idPersonalMedical2, gradUrgenta);
                break;
 
            case 4:               
                System.out.println("(idAnimal, numeAnimal, specie, varsta, masa, numeOwner, telefon");
                int idAnimal4 = keyboard.nextInt();
                String numeAnimal4 = keyboard.next();
                String specie4 = keyboard.next();
                int varsta4 = keyboard.nextInt();
                int masa4 = keyboard.nextInt();
                String numeOwner4 = keyboard.next();
                String telefon4 = keyboard.next();
                
                DatabaseUtil.editAnimalByID(idAnimal4, numeAnimal4, specie4, varsta4, masa4, numeOwner4, telefon4);
                break;

 
            case 5:
                System.out.println("idPersonalMedical, numePersonalMedical, varstaPersonalMedical, String telefon");
                int idPersonalMedical1 = keyboard.nextInt();
                String numePersonalMedical1 = keyboard.next();
                int varstaPersonalMedical1 = keyboard.nextInt();
                String telefon21 = keyboard.next();
 
                DatabaseUtil.editPersonalByID(idPersonalMedical1, numePersonalMedical1, varstaPersonalMedical1, telefon21);
                break;
 
            case 6:
                System.out.println("int idProgramare, Date dataProgramare, int idAnimal, int idPersonalMedical, String gradUrgenta");
 
                int idProgramare3 = keyboard.nextInt();
                int idAnimal3 = keyboard.nextInt();
                int idPersonalMedical3 = keyboard.nextInt();
                String gradUrgenta3 = keyboard.next();
 
                Date dataProgramare = null; //eroare la data
				DatabaseUtil.editProgramareByID(idProgramare3, dataProgramare, idAnimal3, idPersonalMedical3, gradUrgenta3);
                break;
 
            case 7:
                System.out.println("remove AnimalID: ");
                int idAnimalR = keyboard.nextInt();
 
                DatabaseUtil.removeAnimalByID(idAnimalR);
                break;
 
            case 8:
                System.out.println("remove PersonalID: ");
                int personalIdR = keyboard.nextInt();
 
                DatabaseUtil.removePersonalByID(personalIdR);
                break;
 
            case 9:
                System.out.println("remove ProgramareID: ");
                int programareIdR = keyboard.nextInt();
 
                DatabaseUtil.removeProgramareByID(programareIdR);
                break;
 
            case 10:
                DatabaseUtil.printAllAnimalsFromDB();
                break;
 
            case 11:
                DatabaseUtil.printAllPersonalmedicalFromDB();
                break;
 
            case 12:
                DatabaseUtil.printAllProgramareFromDB();
                break;

            default:
                break;
            }
 
        }
		
	     
	}
	
}
	
}


package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public Animal(int idAnimal, String numeAnimal, String specie, int varsta, int masa, String numeOwner, String telefon) {
        this.idAnimal = idAnimal;
        this.numeAnimal = numeAnimal;
        this.specie = specie;
        this.varsta = varsta;
        this.masa = masa;
        this.numeOwner = numeOwner;
        this.telefon = telefon;
    }
	
	
	@Id
	private int idAnimal;

	private float masa;

	private String numeAnimal;

	private String numeOwner;

	private String specie;

	private String telefon;

	private int varsta;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="animal")
	private List<Programare> programares;

	public Animal() {
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public float getMasa() {
		return this.masa;
	}

	public void setMasa(float masa) {
		this.masa = masa;
	}

	public String getNumeAnimal() {
		return this.numeAnimal;
	}

	public void setNumeAnimal(String numeAnimal) {
		this.numeAnimal = numeAnimal;
	}

	public String getNumeOwner() {
		return this.numeOwner;
	}

	public void setNumeOwner(String numeOwner) {
		this.numeOwner = numeOwner;
	}

	public String getSpecie() {
		return this.specie;
	}

	public void setSpecie(String specie) {
		this.specie = specie;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public int getVarsta() {
		return this.varsta;
	}

	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setAnimal(null);

		return programare;
	}

}
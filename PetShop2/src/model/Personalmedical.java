package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idPersonalMedical;

	private String numePersonalMedical;

	private String telefon;

	private int varstaPersonalMedical;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="personalmedical")
	private List<Programare> programares;

	public Personalmedical() {
	}

	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

	public String getNumePersonalMedical() {
		return this.numePersonalMedical;
	}

	public void setNumePersonalMedical(String numePersonalMedical) {
		this.numePersonalMedical = numePersonalMedical;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public int getVarstaPersonalMedical() {
		return this.varstaPersonalMedical;
	}

	public void setVarstaPersonalMedical(int varstaPersonalMedical) {
		this.varstaPersonalMedical = varstaPersonalMedical;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setPersonalmedical(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setPersonalmedical(null);

		return programare;
	}

}